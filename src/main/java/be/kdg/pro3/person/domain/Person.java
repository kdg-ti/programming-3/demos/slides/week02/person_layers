package be.kdg.pro3.person.domain;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Person {
	private String name;
	private int age;
	@SerializedName("only-friend")
	private Person friend;
	private transient List<Person> enemies = new ArrayList<>();


	public Person(String name, int age, Person friend) {
		this.name = name;
		this.age = age;
		this.friend = friend;
	}

	public void addEnemy(Person enemy) {
		enemies.add(enemy);
	}

	public Person getFriend() {
		return friend;
	}

	public void setFriend(Person friend) {
		this.friend = friend;
	}


	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Person{" +
			"name='" + name + '\'' +
			", age=" + age +
			", friend=" + friend +
			", enemies=" + enemies +
			'}';
	}
}
