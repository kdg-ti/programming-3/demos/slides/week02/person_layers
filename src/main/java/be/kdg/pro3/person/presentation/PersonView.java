package be.kdg.pro3.person.presentation;

import be.kdg.pro3.person.domain.Person;
import be.kdg.pro3.person.repository.PersonJsonRepository;
import be.kdg.pro3.person.service.DefaultPersonService;
import be.kdg.pro3.person.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class PersonView {
	private Scanner keyboard = new Scanner(System.in);
	private PersonService svc ;

	@Autowired
	public void setSvc(PersonService svc) {
		this.svc = svc;
	}

	//	public PersonView(PersonService svc) {
//		this.svc = svc;
//	}

	public void showMenu() {
		boolean exit = false;
		while (!exit) {
			System.out.print("""
				What do you want to do?
				1/ Show all persons
				2/ Add a person
				0/ Exit
								
				Please enter the number of your choice: """);
			int choice = keyboard.nextInt();
			keyboard.nextLine();
			switch (choice) {
				case 1 -> showPersons();
				case 2 -> addPerson();
				case 0 -> exit = true;
				default -> System.out.println("Please enter a valid choice");
			}

		}


	}

	private void showPersons() {
		System.out.println(svc.getAll());
	}

	private void addPerson() {
		System.out.print("Please enter the name: ");
		String name = keyboard.nextLine();
		System.out.print("Please enter the age: ");
		int age = keyboard.nextInt();
		svc.add(new Person(name, age));
	}


}
