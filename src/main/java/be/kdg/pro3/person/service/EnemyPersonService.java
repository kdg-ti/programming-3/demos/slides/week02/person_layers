package be.kdg.pro3.person.service;

import be.kdg.pro3.person.domain.Person;
import be.kdg.pro3.person.repository.PersonRepository;
import org.springframework.stereotype.Component;

import java.util.Collection;


public class EnemyPersonService implements PersonService {
	private PersonRepository personRepo;

	public EnemyPersonService(PersonRepository repository) {
		this.personRepo = repository;
	}

	public Person add(Person person){
		person.setFriend(new Person("Tom", 24));
		person.addEnemy(new Person("Lucifer", 666));
		person.addEnemy(new Person("Beelzebub", 6));
		return personRepo.insert(person);
	}

	public Collection<Person> getAll(){
		return personRepo.getAll();
	}
}
