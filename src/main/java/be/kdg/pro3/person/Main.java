package be.kdg.pro3.person;

import be.kdg.pro3.person.domain.Person;
import be.kdg.pro3.person.presentation.PersonView;
import be.kdg.pro3.person.repository.PersonJsonRepository;
import be.kdg.pro3.person.service.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Main {
	public static void main(String[] args) {

		ConfigurableApplicationContext ctx = SpringApplication.run(Main.class, args);
		ctx.getBean(PersonView.class).showMenu();

	//	new PersonView( new EnemyPersonService(new PersonJsonRepository())).showMenu();

	}
}