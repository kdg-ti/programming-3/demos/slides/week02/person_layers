package be.kdg.pro3.person.repository;

import be.kdg.pro3.person.domain.Person;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Component
public class PersonSingleRepository implements PersonRepository {
	private Person data = new Person("Janis", 27);

	@Override
	public Person insert(Person person) {
		data = person;
		return person;
	}

	@Override
	public Collection<Person> getAll() {
		return List.of(data);
	}
}
