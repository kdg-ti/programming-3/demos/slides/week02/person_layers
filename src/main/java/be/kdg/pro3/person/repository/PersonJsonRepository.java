package be.kdg.pro3.person.repository;

import be.kdg.pro3.person.domain.Person;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.Collection;
import java.util.List;

@Repository
@Qualifier("json")
public class PersonJsonRepository implements PersonRepository {
	private static final Logger LOG = LoggerFactory.getLogger(PersonJsonRepository.class);
	private String FILE="persons.json";
	private String SAVED_FILE="persons.json";
	private List<Person> data;
	private Gson gson;

	public PersonJsonRepository(Gson gson) {
//		gson = new GsonBuilder().gsonBuilder.create();

		//System.out.println(">>> reading file " + FILE);
		LOG.warn("Reading file {}" ,FILE);
		this.gson = gson;
		try (BufferedReader reader = new BufferedReader(new FileReader(FILE))) {
			 data = gson.fromJson(reader,new TypeToken<List<Person>>(){}.getType());
		} catch (FileNotFoundException e) {
			LOG.error("File not found");
			throw new RuntimeException(e);
		} catch (IOException e) {
			LOG.error("Something happened while reading the JSON file :(");
			throw new RuntimeException(e);
		}

	}


	@Override
	public Person insert(Person person) {

		data.add(person);
//		try (FileWriter jsonWriter = new FileWriter(SAVED_FILE)) {
//			jsonWriter.write(gson.toJson(data));
//		} catch (IOException e) {
//			throw new RuntimeException(e);
//		}
		return person;
	}

	@Override
	public Collection<Person> getAll() {
		return data;
	}
}
