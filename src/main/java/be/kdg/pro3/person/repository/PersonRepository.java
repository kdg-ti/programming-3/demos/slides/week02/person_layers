package be.kdg.pro3.person.repository;

import be.kdg.pro3.person.domain.Person;

import java.util.Collection;

public interface PersonRepository {
	Person insert(Person person);

	Collection<Person> getAll();
}
