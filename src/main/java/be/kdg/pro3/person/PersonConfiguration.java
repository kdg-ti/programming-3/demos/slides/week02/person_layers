package be.kdg.pro3.person;

import be.kdg.pro3.person.presentation.PersonView;
import be.kdg.pro3.person.repository.PersonJsonRepository;
import be.kdg.pro3.person.repository.PersonRepository;
import be.kdg.pro3.person.service.EnemyPersonService;
import be.kdg.pro3.person.service.PersonService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersonConfiguration {
//	@Bean
//	PersonView personView(PersonService svc){
//		return new PersonView(svc);
//	}
//
//	@Bean
//	PersonService personService(PersonRepository repo){
//		return new EnemyPersonService(repo);
//	}
//
//	@Bean
//	PersonRepository personRepository(){
//		return new PersonJsonRepository();
//	}
	@Bean
	Gson gson(){
		GsonBuilder gsonBuilder = new GsonBuilder();
		return  gsonBuilder.create();
	}
}
