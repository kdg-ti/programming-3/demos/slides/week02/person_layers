package be.kdg.pro3.person.service;

import be.kdg.pro3.person.domain.Person;
import be.kdg.pro3.person.repository.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class DefaultPersonService implements PersonService {
	private PersonRepository personRepo;

	public DefaultPersonService(@Qualifier("json") PersonRepository repository) {
		this.personRepo = repository;
	}

	public Person add(Person person){
		return personRepo.insert(person);
	}

	public Collection<Person> getAll(){
		return personRepo.getAll();
	}
}
