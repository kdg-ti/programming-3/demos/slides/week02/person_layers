package be.kdg.pro3.person.service;

import be.kdg.pro3.person.domain.Person;

import java.util.Collection;

public interface PersonService {

	 Person add(Person person);

	public Collection<Person> getAll();
}
